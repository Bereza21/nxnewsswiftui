//
//  String+Extensions.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 30.01.2022.
//

import Foundation

extension String {
    
    var asUrl: URL? {
        URL(string: self)
    }
}
