//
//  View+Shimmer.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 29.01.2022.
//

import SwiftUI

extension View {
    
    @ViewBuilder
    func shimmer() -> some View {
        modifier(ShimmerModifier())
    }
}

fileprivate struct ShimmerModifier: ViewModifier {
    
    @State private var phase: CGFloat = 0
    
    func body(content: Content) -> some View {
        content.modifier(
            ShimmerAnimatedMaskModifier(phase: phase)
                .animation(.easeOut(duration: 2).repeatForever(autoreverses: false))
        )
        .onAppear {
            phase = 0.8
        }
    }
}

fileprivate struct ShimmerAnimatedMaskModifier: AnimatableModifier {
    var phase: CGFloat
    
    var animatableData: CGFloat {
        get { phase }
        set { phase = newValue }
    }
    
    func body(content: Content) -> some View {
        content
            .mask(ShimmerGradientMaskView(phase: phase).scaleEffect(3))
    }
}

fileprivate struct ShimmerGradientMaskView: View {
    let phase: CGFloat
    let centerColor = Color.white.opacity(0.3)
    let edgeColor = Color.white
    
    var body: some View {
        LinearGradient(
            gradient: Gradient(
                stops: [
                    .init(color: edgeColor, location: phase),
                    .init(color: centerColor, location: phase + 0.1),
                    .init(color: edgeColor, location: phase + 0.2)
                ]
            ),
            startPoint: .leading,
            endPoint: .trailing
        )
    }
}
