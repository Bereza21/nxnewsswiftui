//
//  Button+Style.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 19.01.2022.
//

import SwiftUI

extension Button {
    
    @ViewBuilder
    func styled(_ style: ActionButtonStyle) -> some View {
        switch style {
        case .red:
            buttonStyle(ActionButtonStyleRed())
        case .blue:
            buttonStyle(ActionButtonStyleBlue())
        case .purple:
            buttonStyle(ActionButtonStylePurple())
        case .white:
            buttonStyle(ActionButtonStyleWhite())
        case .whiteBordered:
            buttonStyle(ActionButtonStyleWhiteBordered())
        }
    }
}
