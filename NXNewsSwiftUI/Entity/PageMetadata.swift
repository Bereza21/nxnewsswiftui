//
//  PageMetadata.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 30.01.2022.
//

import Foundation

struct PageMetadata: Codable {
    let page: Int
    let per: Int
    let total: Int
}
