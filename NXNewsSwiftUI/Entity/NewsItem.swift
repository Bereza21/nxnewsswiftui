//
//  NewsItem.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 29.01.2022.
//

import Foundation

struct NewsList: Codable {
    var news: [NewsItem]
    let pageable: PageMetadata
    
    var hasMoreNews: Bool {
        news.count < pageable.total
    }
}

struct NewsItem: Codable, Identifiable {
    let id: String
    let title: String?
    let description: String?
    let detailHtml: String?
    let date: Date?
    private let previewUrl: String?
    var likesCount: Int
    let shareCount: Int
    let commentsCount: Int
    let channel: NewsChannel
    var isLikedByMe: Bool
    
    var previewURL: URL? {
        previewUrl?.asUrl ?? channel.iconUrl?.asUrl
    }
}

extension NewsItem {
    
    private static let loremIpsum = """
    Lorem ipsum dolor sit amet consectetur adipiscing elit donec, gravida commodo hac non mattis augue duis vitae inceptos, laoreet taciti at vehicula cum arcu dictum. Cras netus vivamus sociis pulvinar est erat, quisque imperdiet velit a justo maecenas, pretium gravida ut himenaeos nam. Tellus quis libero sociis class nec hendrerit, id proin facilisis praesent bibendum vehicula tristique, fringilla augue vitae primis turpis.
    Sagittis vivamus sem morbi nam mattis phasellus vehicula facilisis suscipit posuere metus, iaculis vestibulum viverra nisl ullamcorper lectus curabitur himenaeos dictumst malesuada tempor, cras maecenas enim est eu turpis hac sociosqu tellus magnis. Sociosqu varius feugiat volutpat justo fames magna malesuada, viverra neque nibh parturient eu nascetur, cursus sollicitudin placerat lobortis nunc imperdiet. Leo lectus euismod morbi placerat pretium aliquet ultricies metus, augue turpis vulputa
    te dictumst mattis egestas laoreet, cubilia habitant magnis lacinia vivamus etiam aenean.
    """
    
    static let preview: NewsItem = .init(
        id: "1",
        title: "Azazaz",
        description: loremIpsum,
        detailHtml: "azazazaz",
        date: Date(),
        previewUrl: nil,
        likesCount: 1,
        shareCount: 2,
        commentsCount: 3,
        channel: .init(
            id: UUID(),
            url: "",
            iconUrl: nil,
            title: "AZazaz Source",
            siteUrl: nil,
            description: nil,
            createdAt: Date(),
            updatedAt: Date()
        ),
        isLikedByMe: false
    )
}
