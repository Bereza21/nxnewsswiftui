//
//  NewsChannel.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 30.01.2022.
//

import Foundation

struct NewsChannel: Codable {
    let id: UUID
    let url: String
    let iconUrl: String?
    let title: String?
    let siteUrl: String?
    let description: String?
    let createdAt: Date?
    let updatedAt: Date?
}
