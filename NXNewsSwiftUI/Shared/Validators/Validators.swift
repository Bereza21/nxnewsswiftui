//
//  Validators.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 26.01.2022.
//

import Foundation

protocol Validatable: AnyObject {
    func validate(_ text: String) -> String?
}

final class EmptyFieldValidator: Validatable {
    func validate(_ text: String) -> String? {
        text.isEmpty ? "The field is required" : nil
    }
}

final class EmailValidator: Validatable {
    
    func validate(_ text: String) -> String? {
        let regExp = "^.+@.+\\..+$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regExp)
        return predicate.evaluate(with: text) ?  nil : "Email invalid"
    }
}

final class PasswordValidator: Validatable {
    
    func validate(_ text: String) -> String? {
        text.count > 2 ? nil : "Password too weak"
    }
}

final class PasswordMatchValidator: Validatable {
    
    private let matchingValue: String
    
    init(matchingValue: String) {
        self.matchingValue = matchingValue
    }
    
    func validate(_ text: String) -> String? {
        text == matchingValue ? nil : "Password doen't match"
    }
}
