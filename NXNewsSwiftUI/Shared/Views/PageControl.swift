//
//  PageControl.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 16.01.2022.
//

import SwiftUI

struct PageControl: View {
    
    let selectedIndex: Int
    
    let numberOfItems: Int
    
    var body: some View {
        HStack(alignment: .center, spacing: 8) {
            ForEach(0..<numberOfItems) { index in
                Circle()
                    .strokeBorder(index == selectedIndex ? .clear : Asset.Colors.lightGray.color, lineWidth: 1)
                    .background(Circle().foregroundColor(index == selectedIndex ? Asset.Colors.neonRed.color : .clear))
                    .frame(width: 8, height: 8)
            }
        }
    }
}

struct PageControl_Previews: PreviewProvider {
    static var previews: some View {
        PageControl(selectedIndex: 0, numberOfItems: 3).previewLayout(.fixed(width: 375, height: 44))
    }
}
