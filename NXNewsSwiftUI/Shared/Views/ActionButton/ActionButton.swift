//
//  ActionButton.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 16.01.2022.
//

import SwiftUI

struct ActionButton: View {
    
    var style: ActionButtonStyle
    
    var text: String
    
    @Binding var isLoading: Bool
    
    let action: () -> Void
    
    init(
        style: ActionButtonStyle,
        text: String,
        isLoading: Binding<Bool> = .constant(false),
        action: @escaping () -> Void
    ) {
        self.style = style
        self.text = text
        _isLoading = isLoading
        self.action = action
    }
    
    var body: some View {
        Button(action: action) {
            ZStack {
                if !isLoading {
                    Text(text)
                }
                if isLoading {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: style.activityIndicatorColor))
                }
            }
            .frame(
                minWidth: 0,
                maxWidth: .infinity,
                minHeight: 0,
                maxHeight: .infinity,
                alignment: .center
            )
        }
        .styled(style)
        .disabled(isLoading)
        
    }
    
    func tapAction() -> some View {
        self
    }
}

struct ActionButton_Previews: PreviewProvider {
    static var previews: some View {
        ActionButton(style: .red, text: "Next", action: { }).previewLayout(.fixed(width: 343, height: 55))
    }
}
