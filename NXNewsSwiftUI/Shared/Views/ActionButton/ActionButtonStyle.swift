//
//  ActionButtonStyle.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 19.01.2022.
//

import SwiftUI

fileprivate let font: Font = .sfCompactText(.light, size: 16)

fileprivate let pressAlpha: CGFloat = 0.5

enum ActionButtonStyle {
    case red
    case blue
    case purple
    case white
    case whiteBordered
    
    var activityIndicatorColor: Color {
        switch self {
        case .red, .blue, .purple:
            return .white
            
        case .white, .whiteBordered:
            return .gray
        }
    }
}

struct ActionButtonStyleRed: ButtonStyle {
    
    func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .foregroundColor(Asset.Colors.catskillWhite.color)
            .font(font)
            .background(
                configuration.isPressed ? Asset.Colors.neonRed.color.opacity(pressAlpha) : Asset.Colors.neonRed.color
            )
            .cornerRadius(8)
    }
}

struct ActionButtonStyleBlue: ButtonStyle {
    
    func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .foregroundColor(Asset.Colors.catskillWhite.color)
            .font(font)
            .background(
                configuration.isPressed ? Asset.Colors.skyBlue.color.opacity(pressAlpha) : Asset.Colors.skyBlue.color
            )
            .cornerRadius(8)
    }
}

struct ActionButtonStylePurple: ButtonStyle {
    
    func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .foregroundColor(Asset.Colors.catskillWhite.color)
            .font(font)
            .background(
                configuration.isPressed ? Asset.Colors.brightViolet.color.opacity(pressAlpha) : Asset.Colors.brightViolet.color
            )
            .cornerRadius(8)
    }
}

struct ActionButtonStyleWhite: ButtonStyle {
    
    func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .foregroundColor(Asset.Colors.charcoalBlack.color)
            .font(font)
            .background(
                configuration.isPressed ? Asset.Colors.catskillWhite.color.opacity(pressAlpha) : Asset.Colors.catskillWhite.color
            )
            .cornerRadius(8)
    }
}

struct ActionButtonStyleWhiteBordered: ButtonStyle {
    
    func makeBody(configuration: Configuration) -> some View {
        if #available(iOS 15.0, *) {
            configuration
                .label
                .foregroundColor(configuration.isPressed ? .black.opacity(pressAlpha) : .black)
                .font(font)
                .background(Asset.Colors.catskillWhite.color)
                .overlay {
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(configuration.isPressed ? Asset.Colors.charcoalBlack.color.opacity(pressAlpha) : Asset.Colors.charcoalBlack.color, lineWidth: 1)
                }
        } else {
            configuration
                .label
                .foregroundColor(configuration.isPressed ? .black.opacity(pressAlpha) : .black)
                .font(font)
                .background(Asset.Colors.catskillWhite.color)
                .cornerRadius(8)
        }
    }
}
