//
//  FormTextField.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 19.01.2022.
//

import SwiftUI

struct FormTextField: View {
    
    let placeholder: String
    
    var keyboardType: UIKeyboardType = .default
    
    var isSecure: Bool = false
    
    @Binding var text: String
    
    @Binding var error: String?
    
    var body: some View {
        VStack(alignment: .leading, spacing: .zero) {
            ZStack(alignment: .leading) {
                VStack {
                    Spacer()
                    Text(placeholder)
                        .foregroundColor(Asset.Colors.lightGray.color)
                        .font(.sfCompactText(.light, size: 16))
                        .padding([.bottom], (text.isEmpty) ? 0 : 24)
                        .transformEffect((text.isEmpty) ? .identity : .init(scaleX: 12.0 / 15.0, y: 12.0 / 15.0))
                        .animation(.easeOut(duration: 0.2), value: text)
                }
                VStack {
                    Spacer()
                    textField()
                    .frame(height: 20)
                }
            }
            .frame(height: 44)
            .padding([.leading, .trailing], 4)
            .padding(.bottom, 8)
            .background(Color.white)
            
            Divider()
            
            if let error = error {
                Group {
                    Text(error)
                        .foregroundColor(Asset.Colors.neonRed.color)
                        .font(.sfProText(.medium, size: 13))
                        .multilineTextAlignment(.leading)
                        .padding([.leading, .trailing, .top], 4)
                        
                }
                .transition(.move(edge: .top))
                .animation(.easeOut(duration: 0.2))
            }
            
        }
    }
    
    @ViewBuilder
    private func textField() -> some View {
        if isSecure {
            SecureField("", text: $text)
        } else {
            TextField("", text: $text)
                .keyboardType(keyboardType)
        }
    }
}

struct FormTextField_Previews: PreviewProvider {
    static var previews: some View {
        FormTextField(placeholder: "Email", text: .constant(""), error: .constant(nil)).previewLayout(.fixed(width: 343, height: 52))
    }
}
