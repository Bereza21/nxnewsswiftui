//
//  SocialActionButton.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 30.01.2022.
//

import SwiftUI

struct SocialActionButton: View {
    
    let text: String
    
    let image: Image
    
    let imageColor: Color
    
    let action: () -> Void
    
    var body: some View {
        Button(action: action) {
            HStack(spacing: 8) {
                image.accentColor(imageColor)
                Text(text).font(.sfProDisplay(.bold, size: 12))
                    .foregroundColor(Asset.Colors.blueGrey.color)
            }
        }
    }
}
