//
//  ListEmptyView.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 30.01.2022.
//

import SwiftUI

struct ListEmptyView: View {
    
    let text: String
    
    let image: Image
    
    let action: (() -> Void)?
    
    var body: some View {
        VStack(spacing: 32) {
            VStack(spacing: 8) {
                image
                Text(text)
                    .font(.sfProDisplay(.bold, size: 24))
            }

            if let action = action {
                ActionButton(
                    style: .blue,
                    text: "RETRY",
                    action: action
                )
                .frame(width: 320, height: 48, alignment: .center)
            }
        }
    }
}

struct EmptyView_Previews: PreviewProvider {
    static var previews: some View {
        ListEmptyView(
            text: "Oops... Something went wrong",
            image: Asset.Assets.imDefaultError.image,
            action: {}
        )
    }
}
