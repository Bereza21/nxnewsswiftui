//
//  HeaderView.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 19.01.2022.
//

import SwiftUI

struct HeaderView: View {
    
    @State var text: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(text)
                .font(.sfCompactText(.light, size: 36))
                .multilineTextAlignment(.leading)
                .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
            HStack {
                Rectangle()
                    .frame(width: 43, height: 2)
                    .foregroundColor(Asset.Colors.skyBlue.color)
                Spacer()
            }
        }
        
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(text: "Welcome back").previewLayout(.fixed(width: 343, height: 100))
    }
}
