//
//  SocialActionsView.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import SwiftUI

struct SocialActionsView: View {
    
    let likesCount: Int
    
    let isLikedByMe: Bool
    
    let likeTapAllowed: Bool
    
    let shareCount: Int
    
    let didTapLikeButton: () -> Void
    
    let didTapShareButton: () -> Void
    
    var body: some View {
        HStack {
            HStack(alignment: .center, spacing: 32) {
                SocialActionButton(
                    text: "\(likesCount)",
                    image: Asset.Assets.icLike.image,
                    imageColor: isLikedByMe ? Asset.Colors.neonRed.color : Asset.Colors.blueGrey.color,
                    action: didTapLikeButton
                )
                .allowsHitTesting(likeTapAllowed)
                
                SocialActionButton(
                    text: "\(shareCount)",
                    image: Asset.Assets.icShare.image,
                    imageColor: Asset.Colors.blueGrey.color,
                    action: didTapShareButton
                )
            }
            Spacer()
        }
        .padding([.leading, .trailing], 8)
        .frame(height: 52)
    }
}

struct SocialActionsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SocialActionsView(
                likesCount: 2,
                isLikedByMe: true,
                likeTapAllowed: false,
                shareCount: 3,
                didTapLikeButton: {},
                didTapShareButton: {}
            )
            SocialActionsView(
                likesCount: 2,
                isLikedByMe: false,
                likeTapAllowed: false,
                shareCount: 3,
                didTapLikeButton: {},
                didTapShareButton: {}
            )
        }.previewLayout(.fixed(width: 375, height: 44))

    }
}
