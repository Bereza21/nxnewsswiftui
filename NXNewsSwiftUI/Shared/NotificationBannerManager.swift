//
//  NotificationBannerManager.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 10.02.2022.
//

import NotificationBannerSwift
import UIKit

final class NotificationBannerManager {
    
    static let shared: NotificationBannerManager = .init()
    
    func showError(_ error: Error) {
        let banner = FloatingNotificationBanner(title: "Something went wrong", style: .danger)
        showBanner(banner)
    }
    
    private func showBanner(_ banner: FloatingNotificationBanner) {
        banner.show(queuePosition: .front, bannerPosition: .top, queue: .default, on: nil, edgeInsets: UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0), cornerRadius: 8.0, shadowColor: UIColor.black, shadowOpacity: 0.2, shadowBlurRadius: 12.0, shadowCornerRadius: 8.0, shadowOffset: UIOffset(horizontal: 0.0, vertical: 8.0), shadowEdgeInsets: .zero)
    }
}

fileprivate class BannerColorsProvider: BannerColorsProtocol {
    func color(for style: BannerStyle) -> UIColor {
        switch style {
            case .danger:
                return UIColor(named: "lightNeonRed")!
            case .info:
                return UIColor(named: "waterBlue")!
            case .customView:
                return UIColor.clear
            case .success:
                return UIColor(red:0.22, green:0.80, blue:0.46, alpha:1.00)
            case .warning:
                return UIColor(red:1.00, green:0.66, blue:0.16, alpha:1.00)
        }
    }
}
