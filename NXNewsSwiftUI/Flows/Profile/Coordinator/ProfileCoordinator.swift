//
//  ProfileCoordinator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 26.01.2022.
//

import Foundation

final class ProfileCoordinator: CoordinatorProtocol {
    
    private let router: ProfileRouter
    
    init(router: ProfileRouter) {
        self.router = router
    }

    func start() {
        router.openProfile()
    }
}
