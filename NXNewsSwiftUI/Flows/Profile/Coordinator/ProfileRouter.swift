//
//  ProfileRouter.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import UIKit

final class ProfileRouter: RouterProtocol {
    
    internal var nc: UINavigationController
    
    init(nc: UINavigationController) {
        self.nc = nc
    }
    
    func openProfile() {
        let screen = ProfileView()
        nc.push(screen: screen, animated: false)
    }
}
