//
//  OnboardingViewModel.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 16.01.2022.
//

import SwiftUI

typealias OnboardingOut = (OnboardingOutCmd) -> Void

enum OnboardingOutCmd {
    case openLogin
    case openSignUp
    case openTabBar
}

final class OnboardingViewModel: ViewModelProtocol {
    
    private static let texts: [String] = [
        "doloren Lorem ipsum doloren sit amen",
        "sit amen Lorem ipsum doloren sit amen jgfrbcbhj",
        "Lorem ipsum sit amen doloren doloren doloren"
    ]
    
    private static let buttonTexts: [String] = ["NEXT", "NEXT", "CREATE ACCOUNT"]
    
    @Published private(set) var viewState: OnboardingViewState = .init(
        title: texts[0],
        middleButtonText: "NEXT",
        currentPageIndex: 0
    )
    
    private let out: OnboardingOut
    
    init(out: @escaping OnboardingOut) {
        self.out = out
    }
    
    func didTapMiddleButton() {
        guard !viewState.isLastPage else {
            out(.openSignUp)
            return
        }
        
        viewState = OnboardingViewState(
            title: Self.texts[viewState.currentPageIndex + 1],
            middleButtonText: Self.buttonTexts[viewState.currentPageIndex + 1],
            currentPageIndex: viewState.currentPageIndex + 1
        )
    }
    
    func didTapLogin() {
        out(.openLogin)
    }
    
    func didTapContinue() {
        out(.openTabBar)
    }
}
