//
//  OnboardingScreen.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 16.01.2022.
//

import SwiftUI

struct OnboardingScreen<VM: OnboardingViewModel>: Screen {
    
    @StateObject var vm: OnboardingViewModel
    
    var body: some View {
        ZStack() {
            backgroundImage()
            contentView()
        }
        .navigationBarHidden(true)
    }
    
    private func contentView() -> some View {
        VStack(spacing: 48) {
            Spacer()
            titleLabel(text: vm.viewState.title)
            
            VStack(spacing: 8) {
                HStack {
                    Spacer()
                    PageControl(
                        selectedIndex: vm.viewState.currentPageIndex,
                        numberOfItems: 3
                    )
                }
                
                VStack(spacing: 28) {
                    if vm.viewState.isLastPage {
                        loginButton(action: vm.didTapLogin)
                    }

                    middleButton(text: vm.viewState.middleButtonText, action: vm.didTapMiddleButton)
                    
                    if vm.viewState.isLastPage {
                        continueButton(action: vm.didTapContinue)
                    }
                }
            }

        }
        .padding([.leading, .trailing, .bottom], 16)
    }
    
    private func titleLabel(text: String) -> some View {
        Group {
            Text(text)
                .frame(
                    minWidth: .zero,
                    maxWidth: .infinity,
                    alignment: .leading
                )
                .font(.sfProDisplay(.bold, size: 40))
                .foregroundColor(.white)
                .multilineTextAlignment(.leading)
        }
        .transition(.opacity.animation(.linear(duration: 0.2)))
        .animation(.easeOut(duration: vm.viewState.currentPageIndex == 0 ? 0 : 0.2))
        .id("Text-\(vm.viewState.currentPageIndex)")
    }
    
    private func loginButton(action: @escaping () -> Void) -> some View {
        Group {
            ActionButton(
                style: .white,
                text: "LOGIN",
                action: action
            ).frame(
                minWidth: 0,
                maxWidth: .infinity,
                minHeight: 48,
                maxHeight: 48
            )
        }
        .transition(.move(edge: .bottom))
        .animation(.linear(duration: 0.2))
    }
    
    private func middleButton(text: String, action: @escaping () -> Void) -> some View {
        ActionButton(
            style: .red,
            text: text,
            action: action
        ).frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 48,
            maxHeight: 48
        )
    }
    
    private func continueButton(action: @escaping () -> Void) -> some View {
        Group {
            Button("CONTINUE WITHOUT ACCOUNT", action: action)
            .font(.sfCompactText(.light, fixedSize: 16))
            .foregroundColor(Asset.Colors.catskillWhite.color)
        }
        .transition(.move(edge: .top))
        .animation(.linear(duration: 0.2))
    }
    
    private func backgroundImage() -> some View {
        GeometryReader { geometry in
            Asset.Assets.splash.image
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(
                width: geometry.size.width,
                height: geometry.size.height
            )
            .clipped()
        }
        .ignoresSafeArea()
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingScreen(vm: OnboardingViewModel(out: { _ in }))
    }
}
