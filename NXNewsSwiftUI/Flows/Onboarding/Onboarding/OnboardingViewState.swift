//
//  OnboardingViewState.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 31.01.2022.
//

import Foundation

struct OnboardingViewState {
    let title: String
    let middleButtonText: String
    let currentPageIndex: Int
    
    var isLastPage: Bool {
        currentPageIndex == 2
    }
}
