//
//  OnboardingRouter.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import SwiftUI

final class OnboardingRouter: RouterProtocol {
    
    internal let nc: UINavigationController
    
    init(nc: UINavigationController) {
        self.nc = nc
    }
    
    func openOnboarding(out: @escaping OnboardingOut) {
        let vm = OnboardingViewModel(out: out)
        let screen = OnboardingScreen(vm: vm)
        nc.push(screen: screen, animated: true)
    }
}
