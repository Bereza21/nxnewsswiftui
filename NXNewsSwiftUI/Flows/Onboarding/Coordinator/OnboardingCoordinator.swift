//
//  OnboardingCoordinator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 18.01.2022.
//

import UIKit

typealias OnboardingCoordinatorOut = (OnboardingCoordinatorOutCmd) -> Void

enum OnboardingCoordinatorOutCmd {
    case openLogin
    case openSignUp
    case openTabBar
}

final class OnboardingCoordinator: ObservableObject {
    
    private let router: OnboardingRouter
    
    private let out: OnboardingCoordinatorOut
    
    init(router: OnboardingRouter, out: @escaping OnboardingCoordinatorOut) {
        self.router = router
        self.out = out
    }
    
    convenience init(nc: UINavigationController, out: @escaping OnboardingCoordinatorOut) {
        self.init(router: OnboardingRouter(nc: nc), out: out)
    }
    
    func start() {
        router.openOnboarding(out: processOnboarding)
    }
    
    private func processOnboarding(_ cmd: OnboardingOutCmd) {
        switch cmd {
        case .openLogin:
            openLogin()
        case .openSignUp:
            openSignUp()
        case .openTabBar:
            openTabBar()
        }
    }
    
    private func openLogin() {
        out(.openLogin)
    }
    
    private func openSignUp() {
        out(.openSignUp)
    }
    
    private func openTabBar() {
        out(.openTabBar)
    }
}
