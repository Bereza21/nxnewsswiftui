//
//  TabBarCoordinator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 19.01.2022.
//

import UIKit

final class TabBarCoordinator {
    
    private let router: TabBarRouter
    
    private var childCoordinators: [CoordinatorProtocol]!
    
    init(router: TabBarRouter) {
        self.router = router
    }
    
    convenience init(tabBar: UITabBarController) {
        self.init(router: TabBarRouter(tabBar: tabBar))
    }
    
    func start() {
        childCoordinators = router.makeTabBar()
    }
}
