//
//  TabBarRouter.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import UIKit

final class TabBarRouter {
    
    private let tabBarController: UITabBarController
    
    init(tabBar: UITabBarController) {
        self.tabBarController = tabBar
    }
    
    func makeTabBar() -> [CoordinatorProtocol] {
        let newsTab = makeNewsTab()
        let profileTab = makeProfileTab()
        
        tabBarController.setViewControllers([newsTab.0, profileTab.0], animated: false)
        return [newsTab.1, profileTab.1]
    }
    
    func set(tab: TabItem) {
        tabBarController.selectedIndex = tab.rawValue
    }
    
    private func makeNewsTab() -> (UINavigationController, NewsCoordinator) {
        let nc: UINavigationController = .init()
        nc.tabBarItem.image = UIImage(named: "icTabBarNewsUnselected")
        nc.tabBarItem.selectedImage = UIImage(named: "icTabBarNewsSelected")
        nc.tabBarItem.title = "News"
        
        let coordinator: NewsCoordinator = .init(router: NewsRouter(nc: nc))
        coordinator.start()
        return (nc, coordinator)
    }
    
    private func makeProfileTab() -> (UINavigationController, ProfileCoordinator) {
        let nc: UINavigationController = .init()
        nc.tabBarItem.image = UIImage(named: "profileTabBarIconDeselected")
        nc.tabBarItem.selectedImage = UIImage(named: "profileTabBarIconSelected")
        nc.tabBarItem.title = "Profile"
        
        let coordinator: ProfileCoordinator = .init(router: ProfileRouter(nc: nc))
        coordinator.start()
        return (nc, coordinator)
    }
}
