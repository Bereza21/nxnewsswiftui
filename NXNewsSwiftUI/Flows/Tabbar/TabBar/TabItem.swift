//
//  TabItem.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import Foundation

enum TabItem: Int, CaseIterable {
    case news = 0, profile
}
