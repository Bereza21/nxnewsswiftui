//
//  SplashView.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 24.01.2022.
//

import SwiftUI

struct SplashView: View {
    var body: some View {
        Asset.Assets.splash.image
            .resizable()
            .scaledToFill()
            .edgesIgnoringSafeArea(.all)
            .ignoresSafeArea()
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
