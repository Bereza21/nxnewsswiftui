//
//  NewsListViewModel.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 26.01.2022.
//

import Foundation
import SwiftUI

typealias NewsListOut = (NewsListOutCmd) -> Void

enum NewsListOutCmd {
    case openNewsDetails(NewsItem)
    case showAuthAlert
}

final class NewsListViewModel: ViewModelProtocol, EventHandler {
    
    private static let nextPageOffset: Int = 8
    
    private static let pageSize: Int = 24
    
    @Published private(set) var viewState: NewsListViewState = .loading
    
    private let deps: NewsServiceLocator.Dependencies
    
    private let out: NewsListOut
    
    init(deps: NewsServiceLocator.Dependencies, out: @escaping NewsListOut) {
        self.deps = deps
        self.out = out
    }
    
    func obtainEvent(_ event: NewsListEvent) {
        viewState = reduce(event, state: viewState)
    }
    
    private func reduce(_ event: NewsListEvent, state: NewsListViewState) -> NewsListViewState {
        switch event {
        case .viewAppeared:
            return reduceViewAppeared(state)
            
        case .newsLoadedSuccess(let newsList):
            return reduceNewsLoadedSuccess(newsList: newsList, state)
            
        case .newsLoadedFailure:
            return .error
            
        case .didTapRetry:
            return reduceRetryTap(state)
            
        case .newsItemAppeared(let index):
            return reduceNewsItemAppear(index, state: state)
            
        case .didSelectNewsItem(let index):
            return reduceItemSelection(index, state: state)
            
        case .didTapLikeButton(let index):
            return reduceDidTapLikeButton(index, state)
            
        case .likeNewsSuccess(let newsItem):
            return reduceLikeStatusChange(newsItem, state: state)
            
        case .likeNewsFailure(let newsItem):
            return reduceLikeStatusChange(newsItem, state: state)
        }
    }
    
    private func reduceViewAppeared(_ state: NewsListViewState) -> NewsListViewState {
        switch state {
        case .loading:
            getNews(page: 1)
            return .loading
            
        case .content(let data):
            return .content(NewsListContent(
                news: deps.newsService.newsItems,
                page: data.page,
                isNextPageLoading: data.isNextPageLoading,
                isLikeTapEnabled: data.isLikeTapEnabled
            ))
            
        case .error, .contentEmpty:
            return state
        }
    }
    
    private func reduceNewsLoadedSuccess(newsList: NewsList, _ state: NewsListViewState) -> NewsListViewState {
        let isLikeTapEnabled: Bool
        
        switch state {
        case .loading, .contentEmpty, .error:
            isLikeTapEnabled = true
            
        case .content(let data):
            isLikeTapEnabled = data.isLikeTapEnabled
        }
        
        if newsList.news.isEmpty {
            return .contentEmpty
        } else {
            return .content(NewsListContent(
                news: newsList.news,
                page: newsList.pageable,
                isNextPageLoading: false,
                isLikeTapEnabled: isLikeTapEnabled
            ))
        }
    }
    
    private func reduceRetryTap(_ state: NewsListViewState) -> NewsListViewState {
        switch state {
        case .error:
            getNews(page: 1)
            return .loading
            
        case .loading, .content, .contentEmpty:
            assertionFailure()
            return state
        }
    }
    
    private func reduceNewsItemAppear(_ index: Int, state: NewsListViewState) -> NewsListViewState {
        switch state {
        case .content(let data):
            guard data.hasMoreNews,
                  !data.isNextPageLoading
            else {
                return state
            }
            
            if (data.news.count - Self.nextPageOffset) == index {
                getNews(page: data.page.page + 1)
                return .content(NewsListContent(
                    news: data.news,
                    page: data.page,
                    isNextPageLoading: true,
                    isLikeTapEnabled: data.isLikeTapEnabled
                ))
            }
            
            return state
            
        case .error, .loading, .contentEmpty:
            return state
        }
    }
    
    private func reduceItemSelection(_ index: Int, state: NewsListViewState) -> NewsListViewState {
        switch state {
        case .content(let data):
            out(.openNewsDetails(data.news[index]))
            
        case .contentEmpty, .loading, .error:
            assertionFailure()
        }
        
        return state
    }
    
    private func reduceDidTapLikeButton(_ index: Int, _ state: NewsListViewState) -> NewsListViewState {
        switch state {
        case .content(let data):
            guard deps.session.isLoggedIn else {
                out(.showAuthAlert)
                return state
            }
            
            var updatedNews = data.news
            var item = updatedNews[index]
            item.likesCount = item.isLikedByMe ? item.likesCount - 1 : item.likesCount + 1
            item.isLikedByMe.toggle()
            updatedNews[index] = item
            likeNews(item)
            
            return .content(NewsListContent(
                news: updatedNews,
                page: data.page,
                isNextPageLoading: data.isNextPageLoading,
                isLikeTapEnabled: false
            ))
            
        case .contentEmpty, .loading, .error:
            assertionFailure()
            return state
        }
    }
    
    private func reduceLikeStatusChange(_ newsItem: NewsItem, state: NewsListViewState) -> NewsListViewState {
        switch state {
        case .content(let data):
            guard let index = data.news.firstIndex(where: { $0.id == newsItem.id }) else {
                return state
            }
            
            var updatedNews = data.news
            updatedNews[index] = newsItem
            return .content(NewsListContent(
                news: updatedNews,
                page: data.page,
                isNextPageLoading: data.isNextPageLoading,
                isLikeTapEnabled: true
            ))
            
        case .contentEmpty, .loading, .error:
            assertionFailure()
            return state
        }
    }
    
    private func getNews(page: Int) {
        Task {
            do {
                let newsList = try await deps.newsService.fetchNews(pageSize: Self.pageSize, pageOffset: page)
                await MainActor.run {
                    obtainEvent(.newsLoadedSuccess(newsList))
                }
            } catch {
                await MainActor.run {
                    obtainEvent(.newsLoadedFailure(error))
                }
            }
        }
    }
    
    private func likeNews(_ item: NewsItem) {
        Task {
            do {
                let newsItem = try await deps.newsService.likeNews(id: item.id, like: item.isLikedByMe)
                await MainActor.run {
                    obtainEvent(.likeNewsSuccess(newsItem))
                }
            } catch {
                await MainActor.run {
                    obtainEvent(.likeNewsFailure(item))
                }
            }
        }
    }
}
