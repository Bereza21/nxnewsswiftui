//
//  NewsListItemLoadingView.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 26.01.2022.
//

import SwiftUI

struct NewsListItemLoadingView: View {
    
    private static let color: Color = Asset.Colors.veryLightGray.color
    
    @State private var opacity: CGFloat = 0.25
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 8)
                .foregroundColor(.white)
            VStack(spacing: 16) {
                HStack {
                    Circle()
                        .frame(width: 48, height: 48)
                    VStack(spacing: 8) {
                        Rectangle()
                            .cornerRadius(8)
                        Rectangle()
                            .cornerRadius(8)
                    }
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 48, maxHeight: 48)
                }
                Rectangle()
                    .cornerRadius(8)
                Rectangle()
                    .cornerRadius(8)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 24, maxHeight: 24)
            }
            .padding([.all], 16)
            .foregroundColor(Self.color)
            .shimmer()
        }
    }
}

struct NewsListItemLoadingView_Previews: PreviewProvider {
    static var previews: some View {
        NewsListItemLoadingView()
    }
}
