//
//  NewsListItemView.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 29.01.2022.
//

import SwiftUI
import SBPAsyncImage

struct NewsListItemView: View {
    
    private static let animationDuration: Double = 0.15
    
    let newsItem: NewsItem
    
    let isLikeTapAllowed: Bool
    
    let didSelect: () -> Void
    
    let didTapLikeButton: () -> Void
    
    let didTapShareButton: () -> Void
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 8)
                .foregroundColor(.white).shadow(color: Asset.Colors.veryLightGray.color, radius: 6, x: 0, y: 3)
            VStack(alignment: .leading, spacing: 8) {
                topHeaderView(
                    avatarUrl: newsItem.channel.iconUrl?.asUrl,
                    sourceName: newsItem.channel.title,
                    date: newsItem.date
                )
                
                if let title = newsItem.title {
                    Text(title)
                }
                
                centerImageView(previewUrl: newsItem.previewURL)
                actionsView(likesCount: newsItem.likesCount, shareCount: newsItem.shareCount)
            }
            .padding(.all, 8)
        }.onTapGesture {
            didSelect()
        }
    }
    
    @ViewBuilder
    private func topHeaderView(avatarUrl: URL?, sourceName: String?, date: Date?) -> some View {
        HStack {
            BackportAsyncImage(url: avatarUrl) { result in
                switch result {
                case .success(let image):
                    image
                case .failure, .empty:
                    Asset.Assets.placeholder.image
                }
            }
            .frame(width: 44, height: 44)
            .cornerRadius(22)
            VStack(alignment: .leading, spacing: .zero) {
                Text(sourceName ??  "")
                Text("1h 20m ago")
            }
            Spacer()
        }
        .frame(height: 44)
    }
    
    @ViewBuilder
    private func centerImageView(previewUrl: URL?) -> some View {
        BackportAsyncImage(
            url: previewUrl,
            content: { result in
                switch result {
                case .success(let image):
                    image.resizable().scaledToFill()
                default:
                    RoundedRectangle(cornerRadius: 8).foregroundColor(Asset.Colors.veryLightGray.color)
                }
            }
        )
        .frame(height: 212)
        .cornerRadius(8)
        .clipped()
    }
    
    @ViewBuilder
    private func actionsView(likesCount: Int, shareCount: Int) -> some View {
        SocialActionsView(
            likesCount: likesCount,
            isLikedByMe: newsItem.isLikedByMe,
            likeTapAllowed: isLikeTapAllowed,
            shareCount: shareCount,
            didTapLikeButton: didTapLikeButton,
            didTapShareButton: didTapShareButton
        )
    }
}

/*
struct NewsListItem_Previews: PreviewProvider {
    
    static let mewsItem = NewsItem(id: UUID(), title: "", description: "", detailHtml: "", date: Date(), previewUrl: "", likesCount: 0, shareCount: 0, commentsCount: 0, isLikedByMe: false)
    
    static var previews: some View {
        NewsListItemView(newsItem: Self.mewsItem, didTapLikeButton: {}, didTapShareButton: {}).previewLayout(.fixed(width: 372, height: 400))
    }
}*/
