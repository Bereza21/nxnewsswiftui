//
//  NewsListViewState.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 30.01.2022.
//

import Foundation

enum NewsListViewState {
    case loading
    case content(NewsListContent)
    case contentEmpty
    case error
}

struct NewsListContent {
    let news: [NewsItem]
    let page: PageMetadata
    let isNextPageLoading: Bool
    let isLikeTapEnabled: Bool
    
    var hasMoreNews: Bool {
        news.count < page.total
    }
}
