//
//  NewsListEvent.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 30.01.2022.
//

import Foundation

enum NewsListEvent {
    case viewAppeared
    case newsLoadedSuccess(NewsList)
    case newsLoadedFailure(Error)
    case didTapRetry
    case newsItemAppeared(index: Int)
    case didSelectNewsItem(index: Int)
    case didTapLikeButton(index: Int)
    case likeNewsSuccess(NewsItem)
    case likeNewsFailure(NewsItem)
}
