//
//  NewsListScreen.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 26.01.2022.
//

import SwiftUI

struct NewsListScreen<VM: NewsListViewModel>: Screen {
    
    @StateObject var vm: NewsListViewModel
    
    private let columns: [GridItem] = [
        GridItem(spacing: 16)
    ]
    
    var body: some View {
        ZStack {
            Color.white
                .ignoresSafeArea()
            VStack(spacing: 8) {
                HeaderView(text: "Feed")
                    .padding()
                GeometryReader { geometry in
                    ScrollView {
                        switch vm.viewState {
                        case .loading:
                            loadingView(geometry: geometry)
                            
                        case .content(let data):
                            contentView(data: data)
                            
                        case .error:
                            errorView(geometry: geometry)
                            
                        case .contentEmpty:
                            emptyView(geometry: geometry)
                        }
                    }
                    .background(Asset.Colors.backgroudGray.color)
                }
            }
            .navigationBarHidden(true)
            .onAppear {
                vm.obtainEvent(.viewAppeared)
            }
        }
    }
    
    private func loadingView(geometry: GeometryProxy) -> some View {
        LazyVGrid(columns: columns) {
            ForEach(0..<3) { _ in
                NewsListItemLoadingView().frame(height: 320)
            }
        }
        .padding(.all, 16)
    }
    
    private func contentView(data: NewsListContent) -> some View {
        LazyVGrid(columns: columns) {
            ForEach(0..<data.news.count) { index in
                NewsListItemView(
                    newsItem: data.news[index],
                    isLikeTapAllowed: data.isLikeTapEnabled,
                    didSelect: {
                        vm.obtainEvent(.didSelectNewsItem(index: index))
                    },
                    didTapLikeButton: {
                        vm.obtainEvent(.didTapLikeButton(index: index))
                    },
                    didTapShareButton: {}
                ).onAppear {
                    vm.obtainEvent(.newsItemAppeared(index: index))
                }
            }
            
            if data.isNextPageLoading {
                ProgressView()
                    .padding(.all, 16)
            }
        }
        .padding(.all, 16)
    }
    
    private func emptyView(geometry: GeometryProxy) -> some View {
        HStack {
            Spacer()
            ListEmptyView(
                text: "Nothing to display",
                image: Asset.Assets.imDefaultError.image,
                action: nil
            )
            .padding(.all, 16)
            .frame(minHeight: geometry.size.height - 32)
            Spacer()
        }
    }
    
    private func errorView(geometry: GeometryProxy) -> some View {
        HStack {
            Spacer()
            ListEmptyView(
                text: "Oops... Something went wrong",
                image: Asset.Assets.imDefaultError.image,
                action: {
                    vm.obtainEvent(.didTapRetry)
                }
            )
            .frame(minHeight: geometry.size.height - 32)
            .padding(.all, 16)
            Spacer()
        }
    }
}

struct NewsListView_Previews: PreviewProvider {
    static var previews: some View {
        NewsListScreen(vm: NewsListViewModel(deps: NewsServiceLocator.shared, out: { _ in }))
    }
}
