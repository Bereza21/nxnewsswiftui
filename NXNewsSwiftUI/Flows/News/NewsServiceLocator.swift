//
//  NewsServiceLocator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 29.01.2022.
//

import Foundation

final class NewsServiceLocator {
    
    typealias Dependencies = HasNewsService & HasSession
    
    static var shared: Dependencies!
}
