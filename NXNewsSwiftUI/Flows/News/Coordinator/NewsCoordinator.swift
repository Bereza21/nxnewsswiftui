//
//  NewsCoordinator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 26.01.2022.
//

import SwiftUI

final class NewsCoordinator: CoordinatorProtocol {
    
    private let router: NewsRouter
    
    init(router: NewsRouter) {
        self.router = router
    }
    
    func start() {
        router.openNewsList(out: processNewsList)
    }
    
    private func openNewsDetails(_ newsItem: NewsItem) {
        router.openNewsDetails(newsItem: newsItem, out: processNewsDetails)
    }
    
    private func processNewsList(_ cmd: NewsListOutCmd) {
        switch cmd {
        case .openNewsDetails(let newsItem):
            openNewsDetails(newsItem)
            
        case .showAuthAlert:
            router.showAuthAlert()
        }
    }
    
    private func processNewsDetails(_ cmd: NewsDetailsOutCmd) {
        switch cmd {
        case .goBack:
            router.popModule()
            
        case .showAuthAlert:
            router.showAuthAlert()
        }
    }
}
