//
//  NewsRouter.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import UIKit

final class NewsRouter: RouterProtocol {
    
    internal let nc: UINavigationController
    
    init(nc: UINavigationController) {
        self.nc = nc
    }
    
    func openNewsList(out: @escaping NewsListOut) {
        let vm = NewsListViewModel(deps: NewsServiceLocator.shared, out: out)
        let screen = NewsListScreen(vm: vm)
        nc.push(screen: screen, animated: false)
    }
    
    func openNewsDetails(newsItem: NewsItem, out: @escaping NewsDetailsOut) {
        let vm = NewsDetailsViewModel(newsItem: newsItem, deps: NewsServiceLocator.shared, out: out)
        let screen = NewsDetailsScreen(vm: vm)
        nc.push(screen: screen, animated: true, hideBottom: true)
    }
    
    func showAuthAlert() {
        let alert = UIAlertController(title: nil, message: "You must log in to like the news", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        nc.present(alert, animated: true, completion: nil)
    }
}
