//
//  NewsDetailsViewModel.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation

typealias NewsDetailsOut = (NewsDetailsOutCmd) -> Void

enum NewsDetailsOutCmd {
    case goBack
    case showAuthAlert
}

final class NewsDetailsViewModel: ViewModelProtocol {
    
    @Published private(set) var viewState: NewsDetailsViewState
    
    private let deps: NewsServiceLocator.Dependencies
    
    private let out: NewsDetailsOut
    
    init(newsItem: NewsItem, deps: NewsServiceLocator.Dependencies, out: @escaping NewsDetailsOut) {
        self.viewState = NewsDetailsViewState(newsItem: newsItem, likeButtonEnabled: true)
        self.deps = deps
        self.out = out
    }
    
    func didTapLike() {
        guard deps.session.isLoggedIn else {
            out(.showAuthAlert)
            return
        }
        
        likeNews()
    }
    
    func didTapShare() {
        
    }
    
    func didTapBackButton() {
        out(.goBack)
    }
    
    private func likeNews() {
        var item = viewState.newsItem
        item.likesCount = item.isLikedByMe ? item.likesCount - 1 : item.likesCount + 1
        item.isLikedByMe.toggle()
        
        viewState = NewsDetailsViewState(newsItem: item, likeButtonEnabled: false)
        updateNewsItem(item)
    }
    
    private func updateNewsItem(_ item: NewsItem) {
        Task {
            do {
                let newItem = try await deps.newsService.likeNews(id: item.id, like: item.isLikedByMe)
                await MainActor.run {
                    viewState = NewsDetailsViewState(newsItem: newItem, likeButtonEnabled: true)
                }
            } catch {
                await MainActor.run {
                    viewState = NewsDetailsViewState(newsItem: item, likeButtonEnabled: true)
                }
            }
        }
    }
}
