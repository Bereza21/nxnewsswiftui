//
//  NewsDetailsScreen.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import SwiftUI
import SBPAsyncImage

struct NewsDetailsScreen<VM: NewsDetailsViewModel>: Screen {
    
    @StateObject var vm: NewsDetailsViewModel
    
    var body: some View {
        ZStack {
            ScrollView {
                HeaderView(imageUrl: vm.viewState.newsItem.previewURL)
                contentView(newsItem: vm.viewState.newsItem)
            }
            .background(Asset.Colors.catskillWhite.color)
            .ignoresSafeArea(.all)
            backButton(action: vm.didTapBackButton)
        }
        .navigationBarHidden(true)
    }
    
    private struct HeaderView: View {
        
        let imageUrl: URL?
        
        var body: some View {
            GeometryReader { geometry in
                BackportAsyncImage(url: imageUrl) { result in
                    switch result {
                    case .success(let image):
                        image.resizable()
                    case .failure, .empty:
                        Asset.Assets.placeholder.image.resizable()
                    }
                }
                .scaledToFill()
                .frame(
                    width: geometry.size.width,
                    height: height(geometry: geometry)
                )
                .clipped()
                .cornerRadius(12, corners: [.bottomLeft, .bottomRight])
                .offset(x: 0, y: offset(for: geometry))
            }
            .frame(height: 368)
        }
        
        private func height(geometry: GeometryProxy) -> CGFloat {
            let offset = geometry.frame(in: .global).minY
            let imageHeight = geometry.size.height


            if offset > 0 {
                return imageHeight + offset
            }

            return imageHeight
        }
        
        private func offset(for geometry: GeometryProxy) -> CGFloat {
            let offset = geometry.frame(in: .global).minY
            return min(0, -offset)
        }
    }
    
    private func contentView(newsItem: NewsItem) -> some View {
        VStack(spacing: .zero) {
            SocialActionsView(
                likesCount: newsItem.likesCount,
                isLikedByMe: newsItem.isLikedByMe,
                likeTapAllowed: vm.viewState.likeButtonEnabled,
                shareCount: newsItem.shareCount,
                didTapLikeButton: vm.didTapLike,
                didTapShareButton: vm.didTapShare
            )
            Divider()
                .foregroundColor(Asset.Colors.blueGrey.color)
            
            if let description = newsItem.description {
                descriptionLabel(text: description)
            }
        }
    }
    
    private func backButton(action: @escaping () -> Void) -> some View {
        VStack {
            HStack {
                Button(action: action) {
                    Asset.Assets.icBackRounded.image
                        .frame(width: 44, height: 44)
                }
                .shadow(color: .black.opacity(0.2), radius: 8, x: 0, y: 2)
                Spacer()
            }
            Spacer()
        }
    }
    
    private func descriptionLabel(text: String) -> some View {
        Text(vm.viewState.newsItem.description ?? "")
            .foregroundColor(Asset.Colors.charcoalBlack.color)
            .font(.sfProText(.medium, fixedSize: 18))
            .padding(.all, 16)
    }
}

struct NewsDetailsScreen_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NewsDetailsScreen(vm: NewsDetailsViewModel(newsItem: NewsItem.preview, deps: NewsServiceLocator.shared, out: { _ in }))
        }
    }
}
