//
//  NewsDetailsViewState.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation

struct NewsDetailsViewState {
    var newsItem: NewsItem
    var likeButtonEnabled: Bool
}
