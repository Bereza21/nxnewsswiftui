//
//  AuthServiceLocator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation

final class AuthServiceLocator {
    
    typealias Dependencies = HasAuthService & HasSession
    
    static var shared: Dependencies!
}
