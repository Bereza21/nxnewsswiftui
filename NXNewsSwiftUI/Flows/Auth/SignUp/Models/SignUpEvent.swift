//
//  SignUpEvent.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 02.02.2022.
//

import Foundation

enum SignUpEvent {
    case userNameDidChange(String)
    case emailDidChange(String)
    case passwordDidChange(String)
    case signUpButtonTapped
    case loginButtonTapped
    case continueButtonTapped
    case signUpSuccess
    case signUpFailed
}
