//
//  SignUpViewState.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 02.02.2022.
//

import Foundation

struct SignUpViewState {
    var userName: String
    var email: String
    var password: String
    var userNameError: String?
    var emailError: String?
    var passwordError: String?
    var isLoading: Bool
    var isLiveValidationEnabled: Bool
}

extension SignUpViewState {
    
    static let initial: SignUpViewState = .init(
        userName: "",
        email: "",
        password: "",
        userNameError: nil,
        emailError: nil,
        passwordError: nil,
        isLoading: false,
        isLiveValidationEnabled: false
    )
}
