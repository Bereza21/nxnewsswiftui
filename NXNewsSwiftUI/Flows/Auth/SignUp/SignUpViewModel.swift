//
//  SignUpViewModel.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 19.01.2022.
//

import SwiftUI

typealias SignUpOut = (SignUpOutCmd) -> Void

enum SignUpOutCmd {
    case openLodin
    case openTabBar
}

final class SignUpViewModel: ViewModelProtocol, EventHandler {
    
    @Published private(set) var viewState: SignUpViewState = .initial
    
    private let out: SignUpOut
    
    private let deps: AuthServiceLocator.Dependencies
    
    init(deps: AuthServiceLocator.Dependencies, out: @escaping SignUpOut) {
        self.deps = deps
        self.out = out
    }
    
    func obtainEvent(_ event: SignUpEvent) {
        viewState = reduce(event, state: viewState)
    }
    
    private func reduce(_ event: SignUpEvent, state: SignUpViewState) -> SignUpViewState {
        switch event {
        case .emailDidChange(let email):
            return validate(email: email, state: state)
            
        case .passwordDidChange(let password):
            return validate(password: password, state: state)
            
        case .userNameDidChange(let userName):
            return validate(userName: userName, state: state)
            
        case .signUpButtonTapped:
            return validateAndSignUp(state: state)
            
        case .loginButtonTapped, .signUpSuccess:
            out(.openLodin)
            return state
            
        case .continueButtonTapped:
            out(.openTabBar)
            return state
            
        case .signUpFailed:
            return processSignUpFailed(state: state)
        }
    }
    
    private func validate(email: String, state: SignUpViewState) -> SignUpViewState {
        var error: String?
        if state.isLiveValidationEnabled {
            error = EmailValidator().validate(email)
        }
        
        var newState = state
        newState.email = email
        newState.emailError = error
        return newState
    }
    
    private func validate(password: String, state: SignUpViewState) -> SignUpViewState {
        var error: String?
        if state.isLiveValidationEnabled {
            error = PasswordValidator().validate(password)
        }
        
        var newState = state
        newState.password = password
        newState.passwordError = error
        return newState
    }
    
    private func validate(userName: String, state: SignUpViewState) -> SignUpViewState {
        var error: String?
        if state.isLiveValidationEnabled {
            error = EmptyFieldValidator().validate(userName)
        }
        
        var newState = state
        newState.userName = userName
        newState.userNameError = error
        return newState
    }
    
    private func validateAndSignUp(state: SignUpViewState) -> SignUpViewState {
        let emailError: String? = EmailValidator().validate(state.email)
        let passwordError: String? = PasswordValidator().validate(state.password)
        let userNameError: String? = EmptyFieldValidator().validate(state.userName)
        let isValid: Bool = [emailError, passwordError, userNameError].compactMap { $0 }.isEmpty
        
        if isValid {
            signUp(email: state.email, password: state.password, userName: state.userName)
        }
        
        var newState = state
        newState.emailError = emailError
        newState.passwordError = passwordError
        newState.userNameError = userNameError
        newState.isLoading = isValid
        newState.isLiveValidationEnabled = true
        return newState
    }
    
    private func processSignUpFailed(state: SignUpViewState) -> SignUpViewState {
        var newState = state
        newState.isLoading = false
        return newState
    }
    
    private func signUp(email: String, password: String, userName: String) {
        Task {
            do {
                try await deps.authService.register(username: userName, email: email, password: password)
                await MainActor.run {
                    obtainEvent(.signUpSuccess)
                }
            } catch {
                await MainActor.run {
                    obtainEvent(.signUpFailed)
                    NotificationBannerManager.shared.showError(error)
                }
            }
        }
    }
}
