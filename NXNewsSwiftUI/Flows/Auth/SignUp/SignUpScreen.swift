//
//  SignUpScreen.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 19.01.2022.
//

import SwiftUI

struct SignUpScreen<VM: SignUpViewModel>: Screen {
    
    @StateObject var vm: SignUpViewModel
    
    var body: some View {
        ZStack {
            Color.white
                .ignoresSafeArea()
            GeometryReader { geometry in
                ScrollView {
                    VStack {
                        HeaderView(text: "Create\nAccount.")
                        Spacer()
                        VStack(spacing: 48) {
                            VStack(spacing: 32) {
                                userNameTextField()
                                emailTextField()
                                passwordTextField()
                            }
                            signUpButton()
                        }
                        Spacer(minLength: 24)
                        VStack(spacing: 32) {
                            continueButton()
                            loginView()
                        }
                    }.frame(
                        minHeight: geometry.size.height - geometry.safeAreaInsets.bottom - 32,
                        maxHeight: .infinity
                    )
                    .padding(.all, 16)
                }
                .allowsHitTesting(!vm.viewState.isLoading)
                .navigationBarHidden(true)
            }
        }
    }
    
    private func continueButton() -> some View {
        ActionButton(
            style: .whiteBordered,
            text: "CONTINUE WITHOUT ACCOUNT"
        ) {
            vm.obtainEvent(.continueButtonTapped)
        }
        .frame(height: 52)
    }
    
    private func loginView() -> some View {
        HStack {
            Text("Already have an account?")
                .font(.sfCompactText(.light, size: 16))
            Button("Sign In") {
                vm.obtainEvent(.loginButtonTapped)
            }
            .foregroundColor(Asset.Colors.brightViolet.color)
            .font(.sfCompactText(.light, size: 16))
        }
    }
    
    private func signUpButton() -> some View {
        ActionButton(
            style: .blue,
            text: "CREATE ACCOUNT",
            isLoading: Binding<Bool>(
                get: { return self.vm.viewState.isLoading },
                set: { _ in }
            )
        ) {
            vm.obtainEvent(.signUpButtonTapped)
        }
        .frame(height: 52)
    }
    
    private func emailTextField() -> some View {
        FormTextField(
            placeholder: "Email",
            keyboardType: .emailAddress,
            text: Binding<String>(
                get: { return self.vm.viewState.email },
                set: { self.vm.obtainEvent(.emailDidChange($0)) }
            ),
            error: Binding<String?>(
                get: { return self.vm.viewState.emailError },
                set: { _ in }
            )
        )
    }
    
    private func passwordTextField() -> some View {
        FormTextField(
            placeholder: "Password",
            isSecure: true,
            text: Binding<String>(
                get: { return self.vm.viewState.password },
                set: { self.vm.obtainEvent(.passwordDidChange($0)) }
            ),
            error: Binding<String?>(
                get: { return self.vm.viewState.passwordError },
                set: { _ in }
            )
        )
    }
    
    private func userNameTextField() -> some View {
        FormTextField(
            placeholder: "Username",
            text: Binding<String>(
                get: { return self.vm.viewState.userName },
                set: { self.vm.obtainEvent(.userNameDidChange($0)) }
            ),
            error: Binding<String?>(
                get: { return self.vm.viewState.userNameError },
                set: { _ in }
            )
        )
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpScreen(vm: SignUpViewModel(deps: AuthServiceLocator.shared, out: { _ in }))
    }
}
