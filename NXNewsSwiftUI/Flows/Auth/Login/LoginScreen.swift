//
//  LoginScreen.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 17.01.2022.
//

import SwiftUI

struct LoginScreen<VM: LoginViewModel>: Screen {
    
    @StateObject var vm: LoginViewModel
    
    var body: some View {
        ZStack {
            Color.white
                .ignoresSafeArea()
            ScrollView {
                VStack(spacing: 36) {
                    VStack(spacing: 32) {
                        HeaderView(text: "Welcome\nback.")
                        formView()
                    }
                    signInButton()
                        .padding(.bottom, 16)
                    orLabel()
                        .padding(.bottom, 16)
                    signUpButton()
                }
                .padding(.all, 16)
            }
            .allowsHitTesting(!vm.viewState.isLoading)
            .navigationBarHidden(true)
        }
    }
    
    private func formView() -> some View {
        VStack(alignment: .center, spacing: 32) {
            emailTextField()
            passwordTextField()
            HStack {
                Spacer()
                forgotPasswordButton()
            }
        }
    }

    private func orLabel() -> some View {
        Text("OR")
            .font(.sfCompactText(.light, size: 24))
            .foregroundColor(Asset.Colors.charcoalBlack.color)
    }
    
    private func signInButton() -> some View {
        ActionButton(
            style: .purple,
            text: "SIGN IN",
            isLoading: Binding<Bool>(
                get: { return self.vm.viewState.isLoading },
                set: { _ in }
            ),
            action: {
                vm.obtainEvent(.lodinButtonTapped)
            }
        )
        .frame(height: 52)
    }
    
    private func signUpButton() -> some View {
        HStack {
            Text("New user?")
                .font(.sfCompactText(.light, size: 16))
            Button("Sign Up") {
                vm.obtainEvent(.signUpButtonTapped)
            }
            .foregroundColor(Asset.Colors.skyBlue.color)
            .font(.sfCompactText(.light, size: 16))
        }
    }
    
    private func forgotPasswordButton() -> some View {
        Button(action: {
        }, label: {
            Text("Forgot Password?")
                .foregroundColor(Asset.Colors.lightGray.color)
                .font(.sfCompactText(.light, size: 14))
        })
    }
    
    private func emailTextField() -> some View {
        FormTextField(
            placeholder: "Email",
            keyboardType: .emailAddress,
            text: Binding<String>(
                get: { return self.vm.viewState.email },
                set: { self.vm.obtainEvent(.usernameDidChange($0)) }
            ),
            error: Binding<String?>(
                get: { return self.vm.viewState.emailError },
                set: { _ in }
            )
        )
    }
    
    private func passwordTextField() -> some View {
        FormTextField(
            placeholder: "Password",
            isSecure: true,
            text: Binding<String>(
                get: { return self.vm.viewState.password },
                set: { self.vm.obtainEvent(.passwordDidChange($0)) }
            ),
            error: Binding<String?>(
                get: { return self.vm.viewState.passwordError },
                set: { _ in }
            )
        )
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginScreen(vm: LoginViewModel(deps: AuthServiceLocator.shared, out: { _ in }))
    }
}
