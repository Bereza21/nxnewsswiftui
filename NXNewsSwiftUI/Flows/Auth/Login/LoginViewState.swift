//
//  LoginViewState.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 31.01.2022.
//

import Foundation

struct LoginViewState {
    let email: String
    let password: String
    let emailError: String?
    let passwordError: String?
    let isLoading: Bool
    let isLiveValidationEnabled: Bool
}

extension LoginViewState {
    
    static let initial: LoginViewState = .init(
        email: "ww@ww.ru",
        password: "123",
        emailError: nil,
        passwordError: nil,
        isLoading: false,
        isLiveValidationEnabled: false
    )
}
