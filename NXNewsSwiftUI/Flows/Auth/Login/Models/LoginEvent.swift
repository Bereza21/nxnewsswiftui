//
//  LoginEvent.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 31.01.2022.
//

import Foundation

enum LoginEvent {
    case usernameDidChange(String)
    case passwordDidChange(String)
    case lodinButtonTapped
    case signUpButtonTapped
    case loginSucceed
    case loginFailed
}
