//
//  LoginViewModel.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 18.01.2022.
//

import SwiftUI

typealias LoginOut = (LoginOutCmd) -> Void

enum LoginOutCmd {
    case openTabBar
    case openSignUp
}

final class LoginViewModel: ViewModelProtocol, EventHandler {
    
    @Published var viewState: LoginViewState = .initial
    
    private let deps: AuthServiceLocator.Dependencies
    
    private let out: LoginOut
    
    init(deps: AuthServiceLocator.Dependencies, out: @escaping LoginOut) {
        self.deps = deps
        self.out = out
    }
    
    func obtainEvent(_ event: LoginEvent) {
        viewState = reduce(event, state: viewState)
    }
    
    private func reduce(_ event: LoginEvent, state: LoginViewState) -> LoginViewState {
        switch event {
        case .usernameDidChange(let email):
            return validate(email: email, state: state)
            
        case .passwordDidChange(let password):
            return validate(password: password, state: state)
            
        case .lodinButtonTapped:
            return validateAndLogin(state: state)
            
        case .signUpButtonTapped:
            out(.openSignUp)
            return state
            
        case .loginSucceed:
            out(.openTabBar)
            return state
            
        case .loginFailed:
            return processLoginFailure(state: state)
        }
    }
    
    private func validate(email: String, state: LoginViewState) -> LoginViewState {
        var error: String?
        if state.isLiveValidationEnabled {
            error = EmailValidator().validate(email)
        }
        
        return LoginViewState(
            email: email,
            password: state.password,
            emailError: error,
            passwordError: state.passwordError,
            isLoading: state.isLoading,
            isLiveValidationEnabled: state.isLiveValidationEnabled
        )
    }
    
    private func validate(password: String, state: LoginViewState) -> LoginViewState {
        var error: String?
        if state.isLiveValidationEnabled {
            error = PasswordValidator().validate(password)
        }
        
        return LoginViewState(
            email: state.email,
            password: password,
            emailError: state.emailError,
            passwordError: error,
            isLoading: state.isLoading,
            isLiveValidationEnabled: state.isLiveValidationEnabled
        )
    }
    
    private func validateAndLogin(state: LoginViewState) -> LoginViewState {
        let emailError: String? = EmailValidator().validate(state.email)
        let passwordError: String? = PasswordValidator().validate(state.password)
        let isValid: Bool = emailError == nil && passwordError == nil
        
        if isValid {
            login(email: state.email, password: state.password)
        }
        
        return LoginViewState(
            email: state.email,
            password: state.password,
            emailError: emailError,
            passwordError: passwordError,
            isLoading: isValid,
            isLiveValidationEnabled: true
        )
    }
    
    private func processLoginFailure(state: LoginViewState) -> LoginViewState {
        LoginViewState(
            email: state.email,
            password: state.password,
            emailError: state.emailError,
            passwordError: state.passwordError,
            isLoading: false,
            isLiveValidationEnabled: state.isLiveValidationEnabled
        )
    }
    
    private func login(email: String, password: String) {
        Task {
            do {
                let dto = try await deps.authService.login(email: email, password: password)
                await MainActor.run {
                    deps.session.setAuthToken(dto.token)
                    obtainEvent(.loginSucceed)
                }
            } catch {
                await MainActor.run {
                    obtainEvent(.loginFailed)
                    NotificationBannerManager.shared.showError(error)
                }
            }
        }
    }
}
