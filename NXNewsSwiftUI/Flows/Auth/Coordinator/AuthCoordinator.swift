//
//  AuthCoordinator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 18.01.2022.
//

import UIKit

typealias AuthCoordinatorOut = (AuthCoordinatorOutCmd) -> Void

enum AuthCoordinatorOutCmd {
    case openTabBar
}

enum AuthBeginningFlow {
    case login
    case signUp
}

final class AuthCoordinator: ObservableObject {
    
    let bedinningFlow: AuthBeginningFlow
    
    private let router: AuthRouter
    
    private let out: AuthCoordinatorOut
    
    init(beginningFlow: AuthBeginningFlow, router: AuthRouter, out: @escaping AuthCoordinatorOut) {
        self.bedinningFlow = beginningFlow
        self.router = router
        self.out = out
    }
    
    convenience init(beginningFlow: AuthBeginningFlow, nc: UINavigationController, out: @escaping AuthCoordinatorOut) {
        self.init(beginningFlow: beginningFlow, router: AuthRouter(nc: nc), out: out)
    }
    
    func start() {
        switch bedinningFlow {
        case .login:
            router.openLogin(out: processLogin)
            
        case .signUp:
            router.openSignUp(out: processSignUp)
        }
    }
    
    private func processLogin(_ cmd: LoginOutCmd) {
        switch cmd {
        case .openTabBar:
            openTabBar()
            
        case .openSignUp:
            openSignUp()
        }
    }
    
    private func processSignUp(_ cmd: SignUpOutCmd) {
        switch cmd {
        case .openLodin:
            openLogin()
            
        case .openTabBar:
            openTabBar()
        }
    }
    
    private func openSignUp() {
        switch bedinningFlow {
        case .login:
            router.openSignUp(out: processSignUp)
            
        case .signUp:
            router.popModule()
        }
    }
    
    private func openTabBar() {
        out(.openTabBar)
    }
    
    private func openLogin() {
        switch bedinningFlow {
        case .login:
            router.popModule()
            
        case .signUp:
            router.openLogin(out: processLogin)
        }
    }
}
