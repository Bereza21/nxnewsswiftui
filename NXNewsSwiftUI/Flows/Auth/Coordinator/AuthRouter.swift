//
//  AuthRouter.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import Foundation
import UIKit
import SwiftUI

final class AuthRouter: RouterProtocol {
    
    internal let nc: UINavigationController
    
    init(nc: UINavigationController) {
        self.nc = nc
    }
    
    func openLogin(out: @escaping LoginOut) {
        let vm = LoginViewModel(deps: AuthServiceLocator.shared, out: out)
        let screen = LoginScreen(vm: vm)
        nc.push(screen: screen, animated: true)
    }
    
    func openSignUp(out: @escaping SignUpOut) {
        let vm = SignUpViewModel(deps: AuthServiceLocator.shared, out: out)
        let screen = SignUpScreen(vm: vm)
        nc.push(screen: screen, animated: true)
    }
    
    func popModule() {
        nc.popViewController(animated: true)
    }
}
