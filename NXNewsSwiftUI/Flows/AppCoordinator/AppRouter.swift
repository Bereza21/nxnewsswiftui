//
//  AppRouter.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import UIKit

final class AppRouter {
    
    private weak var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func openOnboarding(out: @escaping OnboardingCoordinatorOut) -> OnboardingCoordinator {
        let nc = UINavigationController()
        let coordinator = OnboardingCoordinator(nc: nc, out: out)
        coordinator.start()
        setRootViewController(nc)
        return coordinator
    }
    
    func openAuth(beginningFlow: AuthBeginningFlow, out: @escaping AuthCoordinatorOut) -> AuthCoordinator {
        let nc = UINavigationController()
        let coordinator = AuthCoordinator(beginningFlow: beginningFlow, nc: nc, out: out)
        coordinator.start()
        setRootViewController(nc)
        return coordinator
    }
    
    func openTabBar() -> TabBarCoordinator {
        let tabBar = TabBarController()
        let coordinator = TabBarCoordinator(tabBar: tabBar)
        coordinator.start()
        setRootViewController(tabBar)
        return coordinator
    }
    
    private func setRootViewController(_ vc: UIViewController) {
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        
        if let window = window {
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil)
        }
    }
}
