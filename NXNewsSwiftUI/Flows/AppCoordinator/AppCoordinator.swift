//
//  AppCoordinator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import UIKit

enum Flow {
    case onboarding(OnboardingCoordinator)
    case auth(AuthCoordinator)
    case tabBar(TabBarCoordinator)
}

final class AppCoordinator {
    
    private let router: AppRouter
    
    private var flow: Flow!
    
    init(router: AppRouter) {
        self.router = router
    }
    
    convenience init(window: UIWindow?) {
        self.init(router: AppRouter(window: window))
    }
    
    func start() {
        openOnboarding()
    }
    
    private func openOnboarding() {
        let coordinator = router.openOnboarding(out: processOnboarding)
        flow = .onboarding(coordinator)
    }
    
    private func openLogin() {
        let coordinator = router.openAuth(beginningFlow: .login, out: processAuth)
        flow = .auth(coordinator)
    }
    
    private func openSignUp() {
        let coordinator = router.openAuth(beginningFlow: .signUp, out: processAuth)
        flow = .auth(coordinator)
    }
    
    private func openTabBar() {
        let coordinator = router.openTabBar()
        flow = .tabBar(coordinator)
    }
    
    private func processOnboarding(_ cmd: OnboardingCoordinatorOutCmd) {
        switch cmd {
        case .openLogin:
            openLogin()
            
        case .openSignUp:
            openSignUp()
            
        case .openTabBar:
            openTabBar()
        }
    }
    
    private func processAuth(_ cmd: AuthCoordinatorOutCmd) {
        switch cmd {
        case .openTabBar:
            openTabBar()
        }
    }
}
