//
//  API.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 05.02.2022.
//

import Foundation

struct API {
    
    private static let baseUrl: String = "https://peaceful-coast-25786.herokuapp.com/api/v1/"
    
    static let login: String = Self.baseUrl + "login"
    
    static let register: String = Self.baseUrl + "register"
    
    static let news: String = Self.baseUrl + "news"
    
    static let newsLike: String = Self.baseUrl + "news/like"
}
