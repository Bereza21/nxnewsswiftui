//
//  LikeNewsRequestDTO.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 05.02.2022.
//

import Foundation

struct LikeNewsRequestDTO: Codable {
    let newsId: String
    let like: Bool
}
