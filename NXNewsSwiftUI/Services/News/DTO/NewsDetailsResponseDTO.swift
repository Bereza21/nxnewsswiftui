//
//  NewsDetailsResponseDTO.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 05.02.2022.
//

import Foundation

struct NewsDetailsResponseDTO: Codable {
    let item: NewsItem
}
