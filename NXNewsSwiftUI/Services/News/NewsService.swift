//
//  NewsService.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 29.01.2022.
//

import Foundation
import PomogatorNetworking

protocol HasNewsService: AnyObject {
    var newsService: NewsServiceProtocol { get }
}

protocol NewsServiceProtocol: AnyObject {
    var newsItems: [NewsItem] { get }
    func fetchNews(pageSize: Int, pageOffset: Int) async throws ->  NewsList
    func likeNews(id: String, like: Bool) async throws -> NewsItem
}

final class NewsServie: NewsServiceProtocol {
    
    let session: SessionProtocol
    
    var newsItems: [NewsItem] = []
    
    init(session: SessionProtocol) {
        self.session = session
    }
    
    func fetchNews(pageSize: Int, pageOffset: Int) async throws ->  NewsList {
        let request = BasicRequest<EmptyBody, NewsList>(API.news)
        request.responseDecoder.decoder.dateDecodingStrategy = .iso8601
        if let token = session.token {
            request.headers = ["Authorization": "Bearer \(token)"]
        }
        do {
            let newsList = try await request.execute()
            if pageOffset == 1 {
                self.newsItems = newsList.news
            } else {
                self.newsItems += newsList.news
            }
            return NewsList(news: self.newsItems, pageable: newsList.pageable)
        } catch {
            throw error
        }
    }
    
    func likeNews(id: String, like: Bool) async throws -> NewsItem {
        let request = BasicRequest<LikeNewsRequestDTO, NewsDetailsResponseDTO>(API.newsLike)
        request.method = .post
        request.body = LikeNewsRequestDTO(newsId: id, like: like)
        request.responseDecoder.decoder.dateDecodingStrategy = .iso8601
        if let token = session.token {
            request.headers = ["Authorization": "Bearer \(token)"]
        }
        
        do {
            let dto = try await request.execute()
            guard let index = self.newsItems.firstIndex(where: { $0.id == dto.item.id }) else {
                assertionFailure()
                return dto.item
            }
            
            self.newsItems[index] = dto.item
            return dto.item
        } catch {
            throw error
        }
    }
}
