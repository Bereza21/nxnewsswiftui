//
//  Session.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation

protocol HasSession: AnyObject {
    var session: SessionProtocol { get }
}

protocol SessionProtocol: AnyObject {
    var token: String? { get }
    var isLoggedIn: Bool { get }
    func setAuthToken(_ token: String)
}

final class Session: SessionProtocol {
    
    var token: String? {
        _token
    }
    
    var isLoggedIn: Bool {
        token != nil
    }
    
    private var _token: String?
    
    func setAuthToken(_ token: String) {
        _token = token
    }
}
