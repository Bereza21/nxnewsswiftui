//
//  AuthService.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation
import PomogatorNetworking

protocol HasAuthService: AnyObject {
    var authService: AuthServiceProtocol { get }
}

protocol AuthServiceProtocol: AnyObject {
    @discardableResult
    func register(username: String, email: String, password: String) async throws -> NullResponse
    func login(email: String, password: String) async throws -> LoginResponseDTO
}

final class AuthService: AuthServiceProtocol {
    
    func register(username: String, email: String, password: String) async throws -> NullResponse {
        let request = BasicRequest<SignUpRequestDTO, NullResponse>(API.register)
        request.method = .post
        request.body = SignUpRequestDTO(username: username, email: email, password: password)
        request.responseDecoder.decoder.dateDecodingStrategy = .iso8601
        return try await request.execute()
    }
    
    func login(email: String, password: String) async throws -> LoginResponseDTO {
        let request = BasicRequest<LoginRequestDTO, LoginResponseDTO>(API.login)
        request.method = .post
        request.body = LoginRequestDTO(email: email, password: password)
        request.responseDecoder.decoder.dateDecodingStrategy = .iso8601
        return try await request.execute()
    }
}
