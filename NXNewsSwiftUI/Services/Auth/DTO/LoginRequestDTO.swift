//
//  LoginRequestDTO.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation

struct LoginRequestDTO: Codable {
    let email: String
    let password: String
}
