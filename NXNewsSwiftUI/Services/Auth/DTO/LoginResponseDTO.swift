//
//  LoginResponseDTO.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation

struct LoginResponseDTO: Codable {
    let token: String
}
