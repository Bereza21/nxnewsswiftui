//
//  SignUpRequestDTO.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 04.02.2022.
//

import Foundation

struct SignUpRequestDTO: Codable {
    let username: String
    let email: String
    let password: String
}
