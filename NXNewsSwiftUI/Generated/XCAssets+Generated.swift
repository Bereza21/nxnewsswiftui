// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import SwiftUI

// MARK: - Asset Catalogs


internal enum Asset {
  internal enum Assets {
    internal static let icLike = ImageAsset(name: "icLike")
    internal static let icShare = ImageAsset(name: "icShare")
    internal static let cameraIcon = ImageAsset(name: "cameraIcon")
    internal static let icTabBarNewsSelected = ImageAsset(name: "icTabBarNewsSelected")
    internal static let icTabBarNewsUnselected = ImageAsset(name: "icTabBarNewsUnselected")
    internal static let profileTabBarIconDeselected = ImageAsset(name: "profileTabBarIconDeselected")
    internal static let profileTabBarIconSelected = ImageAsset(name: "profileTabBarIconSelected")
    internal static let close = ImageAsset(name: "close")
    internal static let fb = ImageAsset(name: "fb")
    internal static let google = ImageAsset(name: "google")
    internal static let icBackRounded = ImageAsset(name: "icBackRounded")
    internal static let imDefaultError = ImageAsset(name: "im_default_error")
    internal static let info = ImageAsset(name: "info")
    internal static let placeholder = ImageAsset(name: "placeholder")
    internal static let splash = ImageAsset(name: "splash")
    internal static let twitter = ImageAsset(name: "twitter")
  }
  internal enum Colors {
    internal static let backgroudGray = ColorAsset(name: "backgroudGray")
    internal static let blueGrey = ColorAsset(name: "blueGrey")
    internal static let brightViolet = ColorAsset(name: "brightViolet")
    internal static let catskillWhite = ColorAsset(name: "catskillWhite")
    internal static let charcoalBlack = ColorAsset(name: "charcoalBlack")
    internal static let darkBlue = ColorAsset(name: "darkBlue")
    internal static let denimBlue = ColorAsset(name: "denimBlue")
    internal static let elephantBone = ColorAsset(name: "elephantBone")
    internal static let lightGray = ColorAsset(name: "lightGray")
    internal static let lightNeonRed = ColorAsset(name: "lightNeonRed")
    internal static let neonRed = ColorAsset(name: "neonRed")
    internal static let skyBlue = ColorAsset(name: "skyBlue")
    internal static let veryLightGray = ColorAsset(name: "veryLightGray")
    internal static let waterBlue = ColorAsset(name: "waterBlue")
  }
}

// MARK: - Implementation Details

internal struct ColorAsset {
  fileprivate let name: String

  internal var color: Color {
    Color(self)
  }
}

internal extension Color {
  /// Creates a named color.
  /// - Parameter asset: the color resource to lookup.
  init(_ asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    self.init(asset.name, bundle: bundle)
  }
}

internal struct ImageAsset {
  fileprivate let name: String

  internal var image: Image {
    Image(name)
  }
}

internal extension Image {
  /// Creates a labeled image that you can use as content for controls.
  /// - Parameter asset: the image resource to lookup.
  init(_ asset: ImageAsset) {
    let bundle = Bundle(for: BundleToken.self)
    self.init(asset.name, bundle: bundle)
  }
}

private final class BundleToken {}
