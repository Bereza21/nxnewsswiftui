// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import SwiftUI

extension Font {
  public static func sfCompactText(_ style: SFCompactTextStyle, fixedSize: CGFloat) -> Font {
    return Font.custom(style.rawValue, fixedSize: fixedSize)
  }

  public static func sfCompactText(_ style: SFCompactTextStyle, size: CGFloat, relativeTo textStyle: TextStyle = .body) -> Font {
    return Font.custom(style.rawValue, size: size, relativeTo: textStyle)
  }

  public enum SFCompactTextStyle: String {
    case light = "SFCompactText-Light"
  }
}

extension Font {
  public static func sfProDisplay(_ style: SFProDisplayStyle, fixedSize: CGFloat) -> Font {
    return Font.custom(style.rawValue, fixedSize: fixedSize)
  }

  public static func sfProDisplay(_ style: SFProDisplayStyle, size: CGFloat, relativeTo textStyle: TextStyle = .body) -> Font {
    return Font.custom(style.rawValue, size: size, relativeTo: textStyle)
  }

  public enum SFProDisplayStyle: String {
    case bold = "SFProDisplay-Bold"
  }
}

extension Font {
  public static func sfProText(_ style: SFProTextStyle, fixedSize: CGFloat) -> Font {
    return Font.custom(style.rawValue, fixedSize: fixedSize)
  }

  public static func sfProText(_ style: SFProTextStyle, size: CGFloat, relativeTo textStyle: TextStyle = .body) -> Font {
    return Font.custom(style.rawValue, size: size, relativeTo: textStyle)
  }

  public enum SFProTextStyle: String {
    case bold = "SFProText-Bold"
    case light = "SFProText-Light"
    case medium = "SFProText-Medium"
    case semibold = "SFProText-Semibold"
  }
}
