//
//  AppServiceLocator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 29.01.2022.
//

import Foundation

protocol AppServiceLocatorDependencies: HasNewsService &
                                        HasAuthService &
                                        HasSession {
}

final class AppServiceLocator: AppServiceLocatorDependencies {
    
    static let shared: AppServiceLocator = .init()
    
    private init() {}
    
    lazy var newsService: NewsServiceProtocol = NewsServie(session: session)
    
    lazy var authService: AuthServiceProtocol = AuthService()
    
    lazy var session: SessionProtocol = Session()
}
