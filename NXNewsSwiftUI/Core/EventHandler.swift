//
//  EventHandler.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 31.01.2022.
//

import Foundation

protocol EventHandler: AnyObject {
    associatedtype T
    
    func obtainEvent(_ event: T)
}
