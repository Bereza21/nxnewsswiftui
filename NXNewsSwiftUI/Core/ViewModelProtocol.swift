//
//  ViewModelProtocol.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 07.02.2022.
//

import SwiftUI

protocol ViewModelProtocol: ObservableObject {
    associatedtype State
    var viewState: State { get }
}
