//
//  Router.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import SwiftUI

protocol RouterProtocol: AnyObject {
    var nc: UINavigationController { get }
    
    func popToRootModule(animated: Bool)
    func popToRootModule()
    func popModule()
    func popModule(animtaed: Bool)
}

extension RouterProtocol {
    
    func popToRootModule() {
        popToRootModule(animated: true)
    }
    
    func popToRootModule(animated: Bool) {
        nc.popToRootViewController(animated: animated)
    }
    
    func popModule() {
        popModule(animtaed: true)
    }
    
    func popModule(animtaed: Bool) {
        nc.popViewController(animated: animtaed)
    }
}

extension UINavigationController {
    
    func push<Content: View>(screen: Content, animated: Bool, hideBottom: Bool = false) {
        let controller = UIHostingController(rootView: screen)
        controller.view.backgroundColor = .clear
        controller.hidesBottomBarWhenPushed = hideBottom
        
        pushViewController(controller, animated: animated)
    }
    
    func set<Content: View>(screens: [Content], animated: Bool) {
        let controllers: [UIViewController] = screens.map { screen in
            let controller = UIHostingController(rootView: screen)
            controller.view.backgroundColor = .clear
            return controller
        }
        setViewControllers(controllers, animated: animated)
    }
}
