//
//  Coordinator.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 08.02.2022.
//

import Foundation

protocol CoordinatorProtocol: AnyObject {
    func start()
}
