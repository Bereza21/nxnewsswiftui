//
//  Screen.swift
//  NXNewsSwiftUI
//
//  Created by Vadim Khokhryakov on 07.02.2022.
//

import SwiftUI

protocol Screen: View {
    associatedtype VM: ViewModelProtocol
    
    var vm: VM { get }
}
